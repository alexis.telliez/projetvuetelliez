export class Cocktail {
    constructor( id,name, glassName , category, alcoholic, image ) {
        this.id = id
        this.name = name
        this.glassName = glassName
        this.category = category
        this.alcoholic = alcoholic
        this.image = image
    }
    
}

export class DCocktail{
    constructor( id,name, glassName ,instruction, category, alcoholic, image, ingredients ) {
        this.id = id
        this.name = name
        this.glassName = glassName
        this.instruction = instruction
        this.category = category
        this.alcoholic = alcoholic
        this.image = image
        this.ingredients = ingredients
    }
}