import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Cocktail from '../views/Cocktail.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/cocktail/:id',
    name: 'Cocktail',
    component: Cocktail
  }
]

const router = new VueRouter({
  routes
})

export default router
