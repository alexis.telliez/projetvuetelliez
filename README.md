# projetvuetelliez

Telliez Alexis

## Project setup

Ici, le lien gitlab du projet : https://gitlab.com/alexis.telliez/projetvuetelliez

Pour installer le projet il faut :
```
    git clone https://gitlab.com/alexis.telliez/projetvuetelliez.git
    cd projetvuetelliez
    npm install
    npm run serve
    http://localhost:8080/

```
